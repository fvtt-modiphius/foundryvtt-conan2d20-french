# Localisation française pour CONAN2d20

## Description

Ce module ajoute la terminologie française (non-officielle) au système CONAN2d20 qui adapte les règles de Robert E. Howard's CONAN dans l'application [Foundry VTT](https://foundryvtt.com/packages/conan2d20-french). Le contenu présent dans des compendiums ou autres sources ne sont pas traduit pour le moment.

## Installation

Dans la page Setup/Configuration de votre serveur Foundry VTT:
1. Accéder à la sous-page *Add-on Modules* 
2. Cliquer sur le bouton *Install Module* et copier l'URL suivant dans le champ *Manifest URL:*
    * https://gitlab.com/fvtt-modiphius/foundryvtt-conan2d20-french/-/raw/master/module.json
3. Une fois votre Monde lancé, n'oubliez pas d'activer le module et de définir la langue Française pour ce monde.

## Nous rejoindre

1. Contacter KayhosRaven#1642 sur le Discord de [Foundry VTT](https://discord.gg/foundryvtt).
2. Joindre le Discord du Jeu [CONAN 2D20](https://discord.gg/rbjDnZ9MHV).
3. Créer un [Enjeu/Issue](https://gitlab.com/fvtt-modiphius/foundryvtt-conan2d20-french/-/issues/new) sur le [Gitlab de la traduction](https://gitlab.com/fvtt-modiphius/foundryvtt-conan2d20-french).

## État du module

[![pipeline status](https://gitlab.com/fvtt-modiphius/foundryvtt-conan2d20-french/badges/master/pipeline.svg)](https://gitlab.com/fvtt-modiphius/foundryvtt-conan2d20-french/-/commits/master)
